FROM python:3.7.5
LABEL author="Mahdi"
LABEL description="Dockerfile for Python script which generates emails"
RUN pip install mxnet
COPY . /app
WORKDIR /app
CMD ["python" , "/app/obama_lstm.py"]
