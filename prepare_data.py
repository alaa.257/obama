import os 


# define a function to read a file from a path
def read_content_1(path):
    with open(path) as ins:
        return ins.read()


#Return a dict which maps each char into an unique int id
def build_vocab(path):
    content = list(read_content_1(path))
    idx = 1 # 0 is left for zero-padding
    the_vocab = {}
    for word in content:
        if len(word) == 0:
            continue
        if not word in the_vocab:
            the_vocab[word] = idx
            idx += 1
    return the_vocab


#Encode a sentence with int ids 
def text2id(sentence, the_vocab):
    words = list(sentence)
    return [the_vocab[w] for w in words if len(w) > 0]



#build char vocabulary from input
vocab = build_vocab('obama.txt')   

# create a LSTM Model 
import lstm
# Each line contains at most 129 chars.
seq_len = 129

#embedding dimenstion, which maps a character to a 256-dimension vector
num_embed = 256
# number of lstm lyers
num_lstm_layers = 3

#hidden unit in LSTM Cell 
num_hidden = 512

symbol = lstm.lstm_unroll(
    num_lstm_layers,
    seq_len,
    input_size = len(vocab)+1,
    num_hidden = num_hidden,
    num_embed = num_embed,
    num_label = len(vocab) +1,
    dropout= 0.2)

#########Train 
# First wwe create a Dataiterator 
import bucket_io

# the batch size for training
batch_size = 32

# initialize state for LSTM
init_c = [('l%d_init_c'%l, (batch_size, num_hidden)) for l in range(num_lstm_layers)]
init_h = [('l%d_init_h'%l, (batch_size, num_hidden)) for l in range(num_lstm_layers)]
init_states = init_c + init_h

# Even though BucketSentenceIter support various Length example,
# we simply use the fixed length version here

data_train = bucket_io.BucketSentenceIter(
    path='./obama.txt',
    vocab=vocab,
    buckets=[seq_len],
    batch_size=batch_size,
    init_states=init_states,
    seperate_char='\n',
    text2id= text2id,
    read_content= read_content_1) 
    

# @@@ Autotest_Output_Ignored_Cell
import mxnet as mx
import numpy as np 
import logging

logging.getLogger().setLevel(logging.DEBUG)

# as a Test, we only run 1 epoch. However we can run 100 epoch
num_epoch = 1
#learning rate
learning_rate = 0.01

#Evaluation metric
def Preplexity(label, pred):
    loss = 0.
    for i in range(pred.shape[0]):
        loss += -np.log(max(1e-10, pred[i][int(label[i])]))
    return np.exp(loss/label.size)

model = mx.mod.Module(
    symbol=symbol
)







